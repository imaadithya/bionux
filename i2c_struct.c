#include <stdio.h>
#include <stdlib.h>
#include "i2c_struct.h"
#include <stdint.h>


i2c_struct_t * initialize(){
	i2c_struct_t * i2c_t = malloc(sizeof(i2c_struct_t));
	i2c_t->AC1 = 0;
	i2c_t->AC2 = 0;
	i2c_t->AC3 = 0;
	i2c_t->VB1 = 0;
	i2c_t->VB2 = 0;
	i2c_t->MB = 0;
	i2c_t->MC = 0;
	i2c_t->MD = 0;
	i2c_t->AC4 = 0;
	i2c_t->AC5 = 0;
	i2c_t->AC6 = 0;
	i2c_t->c5 = 0.0;
	i2c_t->c6 = 0.0;
	i2c_t->mc = 0.0;
	i2c_t->md = 0.0;
	i2c_t->x0 = 0.0;
	i2c_t->x1 = 0.0;
	i2c_t->x2 = 0.0;
	i2c_t->Y0 = 0.0;
	i2c_t->Y1 = 0.0;
	i2c_t->y2 = 0.0;
	i2c_t->p0 = 0.0;
	i2c_t->p1 = 0.0;
	i2c_t->p2 = 0.0;
	return i2c_t;
}

void set_AC1(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->AC1 = val;
}
void set_AC2(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->AC2 = val;
}
void set_AC3(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->AC3 = val;
}
void set_VB1(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->VB1 = val;
}
void set_VB2(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->VB2 = val;
}
void set_MB(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->MB = val;
}
void set_MC(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->MC = val;
}
void set_MD(i2c_struct_t * i2c_t, int16_t val){
	i2c_t->MD = val;
}

int16_t get_AC1(i2c_struct_t * i2c_t){
	return i2c_t->AC1;
}
int16_t get_AC2(i2c_struct_t * i2c_t){
	return i2c_t->AC2;
}
int16_t get_AC3(i2c_struct_t * i2c_t){
	return i2c_t->AC3;
}
int16_t get_VB1(i2c_struct_t * i2c_t){
	return i2c_t->VB1;
}
int16_t get_VB2(i2c_struct_t * i2c_t){
	return i2c_t->VB2;
}
int16_t get_MB(i2c_struct_t * i2c_t){
	return i2c_t->MB;
}
int16_t get_MC(i2c_struct_t * i2c_t){
	return i2c_t->MC;
}
int16_t get_MD(i2c_struct_t * i2c_t){
	return i2c_t->MD;
}

void set_AC4(i2c_struct_t * i2c_t, uint16_t val){
	i2c_t->AC4 = val;
}
void set_AC5(i2c_struct_t * i2c_t, uint16_t val){
	i2c_t->AC5 = val;
}
void set_AC6(i2c_struct_t * i2c_t, uint16_t val){
	i2c_t->AC6 = val;
}

uint16_t get_AC4(i2c_struct_t * i2c_t){
	return i2c_t->AC4;
}
uint16_t get_AC5(i2c_struct_t * i2c_t){
	return i2c_t->AC5;
}
uint16_t get_AC6(i2c_struct_t * i2c_t){
	return i2c_t->AC6;
}

void set_c5(i2c_struct_t * i2c_t, double val){
	i2c_t->c5 = val;
}
void set_c6(i2c_struct_t * i2c_t, double val){
	i2c_t->c6 = val;
}
void set_mc(i2c_struct_t * i2c_t, double val){
	i2c_t->mc = val;
}
void set_md(i2c_struct_t * i2c_t, double val){
	i2c_t->md = val;
}
void set_x0(i2c_struct_t * i2c_t, double val){
	i2c_t->x0 = val;
}
void set_x1(i2c_struct_t * i2c_t, double val){
	i2c_t->x1 = val;
}
void set_x2(i2c_struct_t * i2c_t, double val){
	i2c_t->x2 = val;
}

double get_c5(i2c_struct_t * i2c_t){
	return i2c_t->c5;
}
double get_c6(i2c_struct_t * i2c_t){
	return i2c_t->c6;
}
double get_mc(i2c_struct_t * i2c_t){
	return i2c_t->mc;
}
double get_md(i2c_struct_t * i2c_t){
	return i2c_t->md;
}
double get_x0(i2c_struct_t * i2c_t){
	return i2c_t->x0;
}
double get_x1(i2c_struct_t * i2c_t){
	return i2c_t->x1;
}
double get_x2(i2c_struct_t * i2c_t){
	return i2c_t->x2;
}

void set_Y0(i2c_struct_t * i2c_t, double val){
	i2c_t->Y0 = val;
}
void set_Y1(i2c_struct_t * i2c_t, double val){
	i2c_t->Y1 = val;
}
void set_y2(i2c_struct_t * i2c_t, double val){
	i2c_t->y2 = val;
}
void set_p0(i2c_struct_t * i2c_t, double val){
	i2c_t->p0 = val;
}
void set_p1(i2c_struct_t * i2c_t, double val){
	i2c_t->p1 = val;
}
void set_p2(i2c_struct_t * i2c_t, double val){
	i2c_t->p2 = val;
}

double get_Y0(i2c_struct_t * i2c_t){
	return i2c_t->Y0;
}
double get_Y1(i2c_struct_t * i2c_t){
	return i2c_t->Y1;
}
double get_y2(i2c_struct_t * i2c_t){
	return i2c_t->y2;
}
double get_p0(i2c_struct_t * i2c_t){
	return i2c_t->p0;
}
double get_p1(i2c_struct_t * i2c_t){
	return i2c_t->p1;
}
double get_p2(i2c_struct_t * i2c_t){
	return i2c_t->p2;
}
