import time
import mraa
import os
import signal
import multiprocessing

analog = mraa.Aio(0)

emg_stream = 'emg_pipe'
if not os.path.exists(emg_stream):
  os.mkfifo(emg_stream)
pipeout = os.open(emg_stream, os.O_WRONLY)

def pulldata(signum, _):
  try:
    value = str(analog.read()) + "\n"
    os.write(pipeout, value)
  except OSError as e:
    pass 

signal.signal(signal.SIGALRM, pulldata)
signal.setitimer(signal.ITIMER_REAL, 0.01, 0.00099)

while True: 
  signal.pause()
