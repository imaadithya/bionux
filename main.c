#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <mraa.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>
#include "features.h"
#include "fingertip_comm.h"
#include "i2c_struct.h"

#define SAMPLE_COUNT 128
// File
FILE * f;

// Volatile variables modified in ISRs
volatile int counter = 0;
volatile time_t last_time = NULL;
volatile uint16_t emg_count = 0;
volatile uint16_t press_count = 0;
volatile uint16_t accel_count = 0;
volatile uint8_t conversion_ready = 0;
volatile uint8_t lock = 0;
volatile i2c_struct_t * i2c_t;
volatile accel_ready = 0;
volatile uint8_t dac_buf[3];

// Forward directions
int in1_F = 0;
int in2_F = 1;

// Reverse directions
int in1_R = 1;
int in2_R = 0;

// State variables
int state = 0; // State of Arm
int next_state = 0;
int repeat_state = 0;

// Temp variables
int i,c,d;
time_t temp;

// Holder Variables for pressure and acceleration
volatile double T_0, T_1, T_2;
volatile double P_0, P_1, P_2;
volatile int16_t x_0,x_1,x_2;
volatile int16_t y_0,y_1,y_2;
volatile int16_t z_0,z_1,z_2;
volatile int16_t lra_0, lra_1, lra_2;

uint8_t del;
uint8_t res = 3;
double P_filt_0 = 0.0 , P_filt_1 = 0.0, P_filt_2 = 0.0;
double P_filt_0_prev = 0.0, P_filt_1_prev = 0.0, P_filt_2_prev = 0.0;
double P_ang_0 = 0.3, P_ang_1 = 0.6, P_ang_2 = 0.0;
double alpha = 0.98;
volatile uint8_t press_1_flag = 0;
volatile uint8_t press_2_flag = 0;
volatile uint8_t press_3_flag = 0;
volatile uint8_t press_4_flag = 0;
volatile uint8_t press_ready = 0;
volatile uint8_t tran = 0;

// Mutex variables
pthread_t emg_t;
pthread_t accel_t;
pthread_mutex_t fingertip_mutex;

// Timing variables
struct itimerval itime;

int threshold; // Threshold value for Encoder

//FFT variables
volatile double values[SAMPLE_COUNT];
double real[SAMPLE_COUNT];
double imag[SAMPLE_COUNT] = {0.0};
double results[SAMPLE_COUNT] = {0.0};
double fft_out = 0.0;

// MRAA Variables for PWM/GPIO/Analog/I2C
mraa_gpio_context encoder, in1, in2, grasp_switch, finger0_en, finger1_en;
mraa_gpio_context finger2_en;
mraa_pwm_context d1, servo0, servo1, servo2;
mraa_aio_context analog;
mraa_i2c_context * i2c;

void encoder_isr(void* args){
	counter++;
	last_time = time(NULL);
}

void switch_isr(void* args){
	lock = mraa_gpio_read(grasp_switch);
}


void timer_isr(int signum){
	values[emg_count] = (double) mraa_aio_read(analog);
	emg_count ++;
	press_count ++;
	accel_count ++;

	//Acceleration
	if(accel_count == 1){
		accel_ready = 1;
	}

	//Pressure 
	if(press_count == 5 && tran == 0){
		tran = 1;
		press_1_flag = 1;
	}else if(press_count == 26  && tran == 1){
		tran = 0;
		press_2_flag = 1;
	}
	// EMG
	if(emg_count == SAMPLE_COUNT){
		emg_count = 0;
		conversion_ready = 1;
	}
}

void reset(){
	mraa_gpio_write(in1,in1_F);
        mraa_gpio_write(in2,in2_F);
	mraa_pwm_write(d1,0.75);

	counter = 0;		

	threshold = 2000;
	last_time = time(NULL);
	while(counter < threshold){
		temp = time(NULL);
		if(difftime(temp,last_time) > 1){
			counter = threshold;	
		}
	}
	mraa_pwm_write(d1,0.00);
	mraa_gpio_write(in1,in1_R);
	mraa_gpio_write(in2,in2_R);
	mraa_pwm_write(d1,0.75);

	counter = 0;

	threshold = 1800;
	while(counter < threshold);
	mraa_pwm_write(d1,0.00);
	lock = mraa_gpio_read(grasp_switch);
}
void *accel_thread(void *arg){
	while(1){
		if(accel_ready){
			accel_ready = 0;
			/*pthread_mutex_lock(&fingertip_mutex);
			mraa_gpio_write(finger0_en,1);
			mraa_gpio_write(finger1_en,0);
			mraa_gpio_write(finger2_en,0);
			accel_read(&i2c,&x_0,&y_0,&z_0);
			//lra_0 = sqrt((double)(x_0*x_0 + y_0*y_0 + z_0*z_0));
			lra_0 = x_0 + y_0 + z_0;
				
			// Sparkfun DAC doesnt seem to be working
			mraa_i2c_address(i2c, 0x60);
			dac_buf[0] = 0x40;
			dac_buf[1] = lra_0 >> 4;
			dac_buf[2] = (lra_0 & 0xF) << 4;
			mraa_i2c_write(i2c, dac_buf, 3);

			pthread_mutex_unlock(&fingertip_mutex);
			*/
			pthread_mutex_lock(&fingertip_mutex);
			mraa_gpio_write(finger0_en,1);
			mraa_gpio_write(finger1_en,0);
			mraa_gpio_write(finger2_en,0);
			accel_read(&i2c,&x_0,&y_0,&z_0);
			lra_0 = x_0 + y_0 + z_0;
			//printf("%d\n",lra_2);
			// Adafruit DAC #1 works very well
			mraa_i2c_address(i2c, 0x62);
			dac_buf[0] = 0x40;
			dac_buf[1] = lra_0 >> 4;
			dac_buf[2] = (lra_0 & 0xF) << 4;
			mraa_i2c_write(i2c, dac_buf, 3);
			pthread_mutex_unlock(&fingertip_mutex);

			/*pthread_mutex_lock(&fingertip_mutex);
			mraa_gpio_write(finger0_en,0);
			mraa_gpio_write(finger1_en,0);
			mraa_gpio_write(finger2_en,1);
			accel_read(&i2c,&x_2,&y_2,&z_2);
			lra_2 = x_2 + y_2+ z_2;
			//lra_2 = sqrt((double)(x_2*x_2 + y_2*y_2 + z_2*z_2));
	
			// Adafruit DAC #2 Not working
			mraa_i2c_address(i2c, 0x63);
			dac_buf[0] = 0x40;
			dac_buf[1] = lra_0 >> 4;
			dac_buf[2] = (lra_0 & 0xF) << 4;
			mraa_i2c_write(i2c, dac_buf, 3);
			pthread_mutex_unlock(&fingertip_mutex);
			*/
			accel_count = 0;
			//printf("0:%d\n", lra_0);
			//printf("1:%d\n", lra_1);
			//printf("2:%d\n",lra_2);
			//printf("0:%d\n",x_0);
			//printf("1:%d\n",x_1);
			//printf("2:%d\n",lra_2);
			//printf("1:%d\n",lra_2 >> 4);
			//printf("2:%d\n",(lra_2 & 0xF) << 4);
		}
	}
}

void *emg_thread(void *arg){
	while(1){
		if(conversion_ready && !lock){
			memmove(real, values, SAMPLE_COUNT * sizeof(double));
			memcpy(imag,(double[SAMPLE_COUNT]){ 0 }, SAMPLE_COUNT*sizeof(double));
			memcpy(results,(double[SAMPLE_COUNT]){0}, SAMPLE_COUNT*sizeof(double));
			fft_out = 0.0;
			fft_out = FFT(7,real,imag,results);
			//printf("FFT:%f\n",fft_out);
			if((fft_out >=8.0)){
				if(repeat_state == 2){
					next_state = 2;
				}
				repeat_state = 2;
			}else if((fft_out >= 1.2  && fft_out < 3.5)){
				if(repeat_state == 1){
      					next_state = 1;
				}	
				repeat_state = 1;
			}else if(fft_out < 0.65){
				if(repeat_state == 0){
					next_state = 0;
				}
				repeat_state = 0;
			}
			conversion_ready = 0;
		}
		if(next_state == 0 ){
	      		if(state == 1){
				mraa_gpio_write(in1,in1_R);
				mraa_gpio_write(in2,in2_R);
				mraa_pwm_write(d1,0.75);
				
				counter = 0;

				threshold = 800;
				while(counter < threshold);
				mraa_pwm_write(d1,0.00);
				state = 0;
			}else if(state == 2){
				mraa_gpio_write(in1,in1_R);
				mraa_gpio_write(in2,in2_R);
				mraa_pwm_write(d1,0.75);

				counter = 0;

				threshold = 1800;
				while(counter < threshold);
				mraa_pwm_write(d1,0.00);
				state = 0;
			}
		}else if(next_state == 1){
			if(state == 0){
				mraa_gpio_write(in1,in1_F);
				mraa_gpio_write(in2,in2_F);
				mraa_pwm_write(d1,0.75);

				counter = 0;

				threshold = 1000;
				while(counter < threshold);
				mraa_pwm_write(d1,0.00);
				state = 1;
			}else if(state == 2){
				mraa_gpio_write(in1,in1_R);
				mraa_gpio_write(in2,in2_R);
				mraa_pwm_write(d1,0.75);

				counter = 0;

				threshold = 600;
				while(counter < threshold);
				mraa_pwm_write(d1,0.00);
				state = 1;
			}
		}else if(next_state == 2){
			if(state == 0){
				mraa_gpio_write(in1,in1_F);
				mraa_gpio_write(in2,in2_F);
				mraa_pwm_write(d1,0.75);

				counter = 0;

				threshold = 2000;
				last_time = time(NULL);
				while(counter < threshold){
					temp = time(NULL);
					if(difftime(temp,last_time) > 1){
						counter = threshold;
					}	
				}
				mraa_pwm_write(d1,0.00);
				state = 2;
			}else if(state == 1){
				mraa_gpio_write(in1,in1_F);
				mraa_gpio_write(in2,in2_F);
				mraa_pwm_write(d1,0.75);

				counter = 0;
				threshold = 1000;
				last_time = time(NULL);
				while(counter < threshold){
					temp = time(NULL);
					if(difftime(temp,last_time) > 1){
						counter = threshold;
					}
				}
				mraa_pwm_write(d1,0.00);
				state = 2;
			}

		}
	}
}

int main(int argc, char **argv)
{
	// Init MRAA
	mraa_init();

	//f = fopen("lra_dump.txt", "a");

	// Initialize EMG Thread
	//pthread_create(&emg_t, NULL, emg_thread, NULL);
	pthread_create(&accel_t, NULL, accel_thread, NULL);

	// Initialize
	servo0 = mraa_pwm_init(5);
	servo1 = mraa_pwm_init(9);
	servo2 = mraa_pwm_init(3);
	in1 = mraa_gpio_init(8);
	in2 = mraa_gpio_init(7);
	d1 = mraa_pwm_init(6);
	encoder = mraa_gpio_init(4);
	grasp_switch = mraa_gpio_init(0);
	finger0_en = mraa_gpio_init(12);
	finger1_en = mraa_gpio_init(13);
	finger2_en = mraa_gpio_init(2);

	analog = mraa_aio_init(0);

	i2c = mraa_i2c_init(0);

	if(servo0 == NULL){
		printf("servo0 null\n");
	}
	if(servo1 == NULL){
		printf("servo1 null\n");
	}
	if(servo2 == NULL){
		printf("servo2 null\n");
	}
	if(in1 == NULL){
		printf("in1 null\n");
	}
	if(in2 == NULL){
		printf("in2 null\n");
	}
	if(d1 == NULL){
		printf("d1 null\n");
	}
	if(encoder == NULL){
		printf("encoder null\n");
	}
	if(grasp_switch == NULL){
		printf("grasp null\n");
	}
	if(finger0_en == NULL){
		printf("finger0 null\n");
	}
	if(finger1_en == NULL){
		printf("finger1 null\n");
	}
	if(finger2_en == NULL){
		printf("finger2 null\n");
	}
	if(analog == NULL){
		printf("analog null\n");
	}
	if(i2c == NULL){
		printf("i2c null\n");
	}
	
	//PWM Variables for Grasp Motor
	mraa_pwm_period_us(d1, 100);
	mraa_pwm_enable(d1, 1);

	// Testing DACs
	//printf("%d\n", mraa_i2c_address(i2c, 0x60));
	//printf("%d\n", mraa_i2c_write_byte(i2c, 0x00));
	//printf("%d\n", mraa_i2c_address(i2c, 0x62));
	//printf("%d\n", mraa_i2c_write_byte(i2c, 0x00));
	//printf("%d\n", mraa_i2c_address(i2c, 0x63));
	//printf("%d\n", mraa_i2c_write_byte(i2c, 0x00));

	//PWM Variables for Servo (range of 700 to 2300 us)
	mraa_pwm_period_us(servo0, 2300);
	mraa_pwm_enable(servo0, 1);
	mraa_pwm_period_us(servo1, 2300);
	mraa_pwm_enable(servo1, 1);
	mraa_pwm_period_us(servo2, 2300);
	mraa_pwm_enable(servo2, 1);

	// GPIO Directions
	mraa_gpio_dir(in1, MRAA_GPIO_OUT);
	mraa_gpio_dir(in2, MRAA_GPIO_OUT);
	mraa_gpio_dir(finger0_en, MRAA_GPIO_OUT);
	mraa_gpio_dir(finger1_en, MRAA_GPIO_OUT);
	mraa_gpio_dir(finger2_en, MRAA_GPIO_OUT);
	mraa_gpio_dir(encoder, MRAA_GPIO_IN);
	mraa_gpio_dir(grasp_switch, MRAA_GPIO_IN);

	// I2C Setup;
	uint8_t result;
	i2c_t = initialize();

	// Accel and Pressure I2C setup
	for(i = 0; i < 3; i++){
		if(i == 0){
			mraa_gpio_write(finger0_en,1);
			mraa_gpio_write(finger1_en,0);
			mraa_gpio_write(finger2_en,0);
		}else if(i == 1){
			mraa_gpio_write(finger0_en,0);
			mraa_gpio_write(finger1_en,1);
			mraa_gpio_write(finger2_en,0);
		}else if(i == 2){
			mraa_gpio_write(finger0_en,0);
			mraa_gpio_write(finger1_en,0);
			mraa_gpio_write(finger2_en,1);
		}
		accel_power_on(&i2c);
		accel_set_range(&i2c, 16);
		accel_set_bw(&i2c, ADXL345_BW_1600);
		pressure_begin(&i2c, i2c_t);
		start_temperature(&i2c);
	}
	// ISR Setup
	gpio_edge_t edge = MRAA_GPIO_EDGE_BOTH;
	mraa_gpio_isr(encoder, edge, &encoder_isr, NULL);
	mraa_gpio_isr(grasp_switch, edge, &switch_isr, NULL);
	
	//Analog collection using SIGALRM
        signal(SIGALRM, timer_isr);
	itime.it_value.tv_sec = 1;
	itime.it_value.tv_usec = 0;
	itime.it_interval.tv_sec = 0;
	itime.it_interval.tv_usec = 1000;
	setitimer(ITIMER_REAL, &itime, NULL);
	
	//reset();
	
	while(1){
		if(press_1_flag){
			press_1_flag = 0;
			for(i = 0; i < 3; i++){
				if(i == 0){
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,1);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,0);
					get_temperature(&i2c, i2c_t, &T_0);
					pthread_mutex_unlock(&fingertip_mutex);

					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,1);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,0);
					start_pressure(&i2c,res);
					pthread_mutex_unlock(&fingertip_mutex);
				}else if(i == 1){
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,1);
					mraa_gpio_write(finger2_en,0);
					get_temperature(&i2c, i2c_t, &T_1);
					pthread_mutex_unlock(&fingertip_mutex);
					
					pthread_mutex_lock(&fingertip_mutex);
                                        mraa_gpio_write(finger0_en,0);
     					mraa_gpio_write(finger1_en,1);
					mraa_gpio_write(finger2_en,0);
					start_pressure(&i2c,res);
					pthread_mutex_unlock(&fingertip_mutex);
				}else if(i == 2){
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,1);
					get_temperature(&i2c, i2c_t, &T_2);
					pthread_mutex_unlock(&fingertip_mutex);
					
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,1);
					start_pressure(&i2c,res);
					pthread_mutex_unlock(&fingertip_mutex);
				}
			}
			press_count = 0;
		}else if(press_2_flag){
			press_2_flag = 0;
			for(i = 0; i < 3; i++){
				if(i == 0){
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,1);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,0);
					get_pressure(&i2c, i2c_t,  &T_0, &P_0);
					pthread_mutex_unlock(&fingertip_mutex);
					
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,1);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,0);
					start_temperature(&i2c);
					pthread_mutex_unlock(&fingertip_mutex);

					P_filt_0_prev = P_filt_0;
					P_filt_0 = (.1*P_0*alpha) + (1-alpha)*P_filt_0_prev;
					P_ang_0 = (P_filt_0 /10.0) - 12.4;
					printf("P0:%f\n",P_ang_0); 
					if(P_ang_0 > 0.0){
						mraa_pwm_write(servo2, P_ang_0);
					}
				}else if(i == 1){
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,1);
					mraa_gpio_write(finger2_en,0);
					get_pressure(&i2c, i2c_t,  &T_1, &P_1);
					pthread_mutex_unlock(&fingertip_mutex);

					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,1);
					mraa_gpio_write(finger2_en,0);
					start_temperature(&i2c);
					pthread_mutex_unlock(&fingertip_mutex);

					P_filt_1_prev = P_filt_1;
					P_filt_1 = (.1*P_1*alpha) + (1-alpha)*P_filt_1_prev;
					P_ang_1 = 1.0 -((P_filt_1 /10.0) - 16.5);
					//printf("P1:%f\n", P_ang_1);
					if(P_ang_1 > 0.0){
						mraa_pwm_write(servo1, P_ang_1);
					}
				}else if(i == 2){
					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,1);
					get_pressure(&i2c, i2c_t,  &T_2, &P_2);
					pthread_mutex_unlock(&fingertip_mutex);

					pthread_mutex_lock(&fingertip_mutex);
					mraa_gpio_write(finger0_en,0);
					mraa_gpio_write(finger1_en,0);
					mraa_gpio_write(finger2_en,1);
					start_temperature(&i2c);
					pthread_mutex_unlock(&fingertip_mutex);

					P_filt_2_prev = P_filt_2;
					P_filt_2 = (.1*P_2*alpha) + (1-alpha)*P_filt_2_prev;
					P_ang_2 = (P_filt_2 /10.0) - 10.9;
 					printf("P2:%f\n", P_ang_2);
					if(P_ang_2 > 0.0 && P_ang_2 < 0.65){
						mraa_pwm_write(servo0, P_ang_2);
					}
					//printf("0:%f\n",P_ang_0);
					//printf("0:%d\n",lra_0);
					//printf("1:%f\n",P_ang_1);
					//printf("1:%d\n",lra_1);
					//printf("2:%d\n",lra_2);
					//fprintf(f,"%f\n",P_ang_2);
					//printf("a\n");
					//mraa_pwm_write(servo0, P_ang_2);
				}
			}
			press_count = 0;
		}
	}
}
