#ifndef __I2C_STRUCT_H__
#define __I2C_STRUCT_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

typedef struct{
	int16_t AC1,AC2,AC3,VB1,VB2,MB,MC,MD;
	uint16_t AC4,AC5,AC6;
	double c5,c6,mc,md,x0,x1,x2;
	double Y0,Y1,y2,p0,p1,p2;
}i2c_struct_t;

i2c_struct_t * initialize();

void set_AC1(i2c_struct_t * i2c_t, int16_t val);
void set_AC2(i2c_struct_t * i2c_t, int16_t val);
void set_AC3(i2c_struct_t * i2c_t, int16_t val);
void set_VB1(i2c_struct_t * i2c_t, int16_t val);
void set_VB2(i2c_struct_t * i2c_t, int16_t val);
void set_MB(i2c_struct_t * i2c_t, int16_t val);
void set_MC(i2c_struct_t * i2c_t, int16_t val);
void set_MD(i2c_struct_t * i2c_t, int16_t val);

int16_t get_AC1(i2c_struct_t * i2c_t);
int16_t get_AC2(i2c_struct_t * i2c_t);
int16_t get_AC3(i2c_struct_t * i2c_t);
int16_t get_VB1(i2c_struct_t * i2c_t);
int16_t get_VB2(i2c_struct_t * i2c_t);
int16_t get_MB(i2c_struct_t * i2c_t);
int16_t get_MC(i2c_struct_t * i2c_t);
int16_t get_MD(i2c_struct_t * i2c_t);

void set_AC4(i2c_struct_t * i2c_t, uint16_t val);
void set_AC5(i2c_struct_t * i2c_t, uint16_t val);
void set_AC6(i2c_struct_t * i2c_t, uint16_t val);

uint16_t get_AC4(i2c_struct_t * i2c_t);
uint16_t get_AC5(i2c_struct_t * i2c_t);
uint16_t get_AC6(i2c_struct_t * i2c_t);

void set_c5(i2c_struct_t * i2c_t, double val);
void set_c6(i2c_struct_t * i2c_t, double val);
void set_mc(i2c_struct_t * i2c_t, double val);
void set_md(i2c_struct_t * i2c_t, double val);
void set_x0(i2c_struct_t * i2c_t, double val);
void set_x1(i2c_struct_t * i2c_t, double val);
void set_x2(i2c_struct_t * i2c_t, double val);

double get_c5(i2c_struct_t * i2c_t);
double get_c6(i2c_struct_t * i2c_t);
double get_mc(i2c_struct_t * i2c_t);
double get_md(i2c_struct_t * i2c_t);
double get_x0(i2c_struct_t * i2c_t);
double get_x1(i2c_struct_t * i2c_t);
double get_x2(i2c_struct_t * i2c_t);

void set_Y0(i2c_struct_t * i2c_t, double val);
void set_Y1(i2c_struct_t * i2c_t, double val);
void set_y2(i2c_struct_t * i2c_t, double val);
void set_p0(i2c_struct_t * i2c_t, double val);
void set_p1(i2c_struct_t * i2c_t, double val);
void set_p2(i2c_struct_t * i2c_t, double val);

double get_Y0(i2c_struct_t * i2c_t);
double get_Y1(i2c_struct_t * i2c_t);
double get_y2(i2c_struct_t * i2c_t);
double get_p0(i2c_struct_t * i2c_t);
double get_p1(i2c_struct_t * i2c_t);
double get_p2(i2c_struct_t * i2c_t);
#endif
