import time

collected_data = []
pipein = open('emg_pipe', 'r')

for i in range(0, 3):
  if i == 0:
    print "Please relax your muscle. Recording will start in 2 seconds."
  elif i == 1:
    print "Please switch to a 'medium' level of muscle activity. Recording will start in 2 seconds."
  elif i == 2:
    print "Please switch to a 'high' level of muscle activity. Recording will start in 2 seconds."
  
  time.sleep(2.0)
  classification = str(i) + ","

  print "Data is being recorded."
  starting_time = time.time();

  counter = 0
  while True:
	  value = classification + pipein.readline()
	  collected_data.append(value)
	  counter = counter + 1
	  if counter > 9999:
		  break
   
  print "Data recording has stopped."
  print str(time.time() - starting_time)

  if i == 2:
    print "Your data will be ready for you in the designated file."
    f = open("data.txt", "w")
    f.write(''.join(collected_data))
    f.close()  
