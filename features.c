#include "features.h"
#include <stdint.h>
#include <stdio.h>
#include <math.h>

uint16_t mean(uint16_t * array, int len){
	uint16_t i;
	uint64_t sum = 0;

	for(i = 0; i < len; i++){
		sum += array[i];
	}

	return (sum/len);
}

uint16_t max(uint16_t * array, int len){
	uint16_t i;
	uint16_t max = 0;

	for(i = 0; i < len; i++){
		if(array[i] > max){
			max = array[i];
		}
	}
	return max;
}

uint16_t min(uint16_t * array, int len){
	uint16_t i;
	uint16_t min = 65535;

	for(i = 0; i < len; i++){
		if(array[i] < min){
			min = array[i];
		}
	}
	return min;
}

uint16_t range(uint16_t * array, int len){
	return max(array,len) - min(array,len);
}
/*
uint16_t maxll(uint16_t * array, int len){
	uint16_t i;
	uint16_t max = 0;
	uint16_t abs_diff;

	for(i = 0; i < len-1; i++){
		abs_diff = abs(array[i]-array[i+1]);
		if(abs_diff > max){
			max = abs_diff;
		}
	}
	return max;
} */

/*
uint16_t avgll(uint16_t * array, int len){
	uint16_t i;
	uint32_t sum = 0;

	for(i = 0; i < len-1; i++){
		sum += abs(array[i] - array[i+1]);
	}
	return (sum/len);
}*/

float entropy(uint16_t * array, int len){
	int i;
	float final_entropy = 0.0;

	for(i =0; i < len; i++){
		if(array[i] > 0.0){
			final_entropy -= (float)array[i] * (float) log((double) array[i]);
		}
	}
	final_entropy = final_entropy / (float) log((double) 2.0);

	return final_entropy;
}

double FFT(int m, double *x, double *y, double *result){
	int n,i,i1,i2,j,k,l2,l,l1;
	double c1,c2,u1,u2,t1,t2,z,tx,ty;
	double max = 0.0;

	n = 1;
	for(i = 0; i<m; i++){
	       n *= 2;
	}
		
	i2 = n >> 1;

	j = 0;
	for(i= 0; i < n-1;i++){
		if(i < j){
			tx = x[i];
			ty = y[i];
			x[i] = x[j];
			y[i] = y[j];
			x[j] = tx;
			y[j] = ty;
		}
		k = i2;
		while(k <= j){
			j -= k;
			k >>= 1;
		}
		j += k;
	}

	c1 = -1.0;
	c2 = 0.0;
	l2 = 1;
	for(l=0;l<m;l++){
		l1 = l2;
		l2 <<= 1;
		u1 = 1.0;
		u2 = 0.0;
		for(j=0;j<l1;j++){
			for(i=j;i<n;i+=l2){
				i1 = i + l1;
				t1 = u1 * x[i1] - u2 * y[i1];
				t2 = u1 * y[i1] + u2 * x[i1];
				x[i1] = x[i] - t1;
				y[i1] = y[i] - t2;
				x[i] += t1;
				y[i] += t2;
			}
			z = u1 * c1 - u2 * c2;
			u2 = u1 * c2 + u2 * c1;
			u1 = z;
		}
		c2 = -1 * sqrt((1 - c1) / 2.0);;
		c1 = sqrt((1 + c1) / 2.0);
	}
	max = 0.0;
	for(i=1;i<20;i++){
		x[i] /= n;
		y[i] /= n;
		result[i] = sqrt(x[i] * x[i] + y[i] * y[i]);
		max += (result[i] * result[i]);
	}
	return max;
}
			       	


