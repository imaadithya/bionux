#ifndef FEATURES_H
#define FEATURES_H

#include <stdint.h>

uint16_t mean(uint16_t *, int len);
uint16_t max(uint16_t *, int len);
uint16_t min(uint16_t *, int len);
uint16_t range(uint16_t*, int len);
uint16_t maxll(uint16_t*, int len);
uint16_t avgll(uint16_t*, int len);
float entropy(uint16_t*, int len);
double FFT(int m, double *x, double *y, double *result);

#endif
