#ifndef FINGERTIP_COMM_H
#define FINGERTIP_COMM_H

#include <mraa.h>
#include "i2c_struct.h"

// Barometer Defines
#define BMP180_ADDR 0x77

#define BMP180_REG_CONTROL 0xF4
#define BMP180_REG_RESULT 0xF6

#define BMP180_COMMAND_TEMPERATURE 0x2E
#define BMP180_COMMAND_PRESSURE0 0x34 // Low-power
#define BMP180_COMMAND_PRESSURE1 0x74 // Standard
#define BMP180_COMMAND_PRESSURE2 0xB4 // High Resolution
#define BMP180_COMMAND_PRESSURE3 0xF4 // Ultra-High Resolution

// Accelerometer Defines
#define ADXL345_DEVICE 0x53
#define ADXL345_POWER_CTL 0x2D 
#define ADXL345_DATA_FORMAT 0x31
#define ADXL345_BW_RATE 0x2C
#define ADXL345_DATAX0 0x32

#define ADXL345_BW_1600 0xF // 1111
#define ADXL345_BW_800 0xE // 1110
#define ADXL345_BW_400 0xD // 1101 
#define ADXL345_BW_200 0xC // 1100
#define ADXL345_BW_100 0xB // 1011 
#define ADXL345_BW_50  0xA // 1010 
#define ADXL345_BW_25  0x9 // 1001 
#define ADXL345_BW_12  0x8 // 1000 
#define ADXL345_BW_6  0x7 // 0111
#define ADXL345_BW_3  0x6 // 0110

// Accelerometer Functions
void accel_power_on(mraa_i2c_context * i2c);
void accel_set_range(mraa_i2c_context * i2c, uint16_t scale);
void accel_set_bw(mraa_i2c_context * i2c, uint8_t bw);
void accel_read(mraa_i2c_context * i2c, int16_t * x, int16_t * y, int16_t * z);

// Baromter Functions
uint8_t start_temperature(mraa_i2c_context * i2c);
uint8_t get_temperature(mraa_i2c_context * i2c, i2c_struct_t * i2c_t, double * T);
uint8_t pressure_begin(mraa_i2c_context *i2c, i2c_struct_t * i2c_t);
uint8_t start_pressure(mraa_i2c_context * i2c, uint8_t res);
uint8_t get_pressure(mraa_i2c_context * i2c, i2c_struct_t * i2c_t, double * T, double * P);
uint8_t read_uint(mraa_i2c_context * i2c, uint8_t address, uint16_t * value);
uint8_t read_int(mraa_i2c_context * i2c, uint8_t address, int16_t * value);

// I2C Mux Functions
void select_i2c_channel(mraa_i2c_context * i2c, uint8_t channel);

#endif
