#include <mraa.h>
#include <math.h>
#include "fingertip_comm.h"

uint8_t read_int(mraa_i2c_context * i2c, uint8_t address, int16_t * value){
	uint8_t data[2];

	mraa_i2c_write_byte(*i2c,address);
	if(mraa_i2c_read(*i2c, data, 2)){
		*value = (((int16_t)data[0]<<8)|(int16_t)data[1]);
		return 1;
	}
	*value = 0;
	return 0;
}

uint8_t read_uint(mraa_i2c_context * i2c, uint8_t address, uint16_t * value){
	uint8_t data[2];

	mraa_i2c_write_byte(*i2c,address);
	if(mraa_i2c_read(*i2c, data, 2)){
		*value = (((uint16_t)data[0]<<8)|(uint16_t)data[1]);
		return 1;
	}
	*value = 0;
	return 0;
}

void accel_power_on(mraa_i2c_context * i2c){
	uint8_t data[2];
	data[0] = ADXL345_POWER_CTL;
	data[1] = 0;
	mraa_i2c_address(*i2c, ADXL345_DEVICE);
	mraa_i2c_write(*i2c, data, 2);
	data[1] = 16;
	mraa_i2c_write(*i2c, data, 2);
	data[1] = 8;
	mraa_i2c_write(*i2c, data, 2);
}	
void accel_set_range(mraa_i2c_context * i2c, uint16_t scale){
	uint8_t s;
	uint8_t b;

	switch(scale){
		case 2:
			s = 0x00;
			break;
		case 4:
			s = 0x01;
			break;
		case 8:
			s = 0x02;
			break;
		case 16:
			s = 0x03;
			break;
		default:
			s = 0x00;
			break;
	}
	mraa_i2c_address(*i2c, ADXL345_DEVICE);
	mraa_i2c_write_byte(*i2c, ADXL345_DATA_FORMAT);
	mraa_i2c_read(*i2c, &b, 1);
	mraa_i2c_write_byte(*i2c, ADXL345_DATA_FORMAT);
	mraa_i2c_write_byte(*i2c, b);
}
void accel_set_bw(mraa_i2c_context *i2c, uint8_t bw){
	mraa_i2c_address(*i2c, ADXL345_DEVICE);
	mraa_i2c_write_byte(*i2c, ADXL345_BW_RATE);
	mraa_i2c_write_byte(*i2c, bw);
}
void accel_read(mraa_i2c_context * i2c, int16_t * x, int16_t * y, int16_t * z){
	uint8_t buff[6];
	mraa_i2c_address(*i2c, ADXL345_DEVICE);
	mraa_i2c_write_byte(*i2c, ADXL345_DATAX0);
	mraa_i2c_read(*i2c, buff, 6);
	*x = (((int16_t) buff[1]) << 8) | buff[0];
	*y = (((int16_t) buff[3]) << 8) | buff[2];
	*z = (((int16_t) buff[5]) << 8) | buff[4];
}

void select_i2c_channel(mraa_i2c_context * i2c, uint8_t channel){
	mraa_i2c_address(*i2c, 0x70);
	switch(channel){
		case 0:
			mraa_i2c_write_byte(*i2c, 0x01);
			break;
		case 1:
			mraa_i2c_write_byte(*i2c, 0x02);
			break;
		case 2:
			mraa_i2c_write_byte(*i2c, 0x04);
			break;
		case 3:
			mraa_i2c_write_byte(*i2c, 0x08);
			break;
		case 4:
			mraa_i2c_write_byte(*i2c, 0x10);
			break;
		default:
			mraa_i2c_write_byte(*i2c, 0x01);
			break;
	}
}

uint8_t pressure_begin(mraa_i2c_context * i2c, i2c_struct_t * i2c_t){
	/*int16_t AC1,AC2,AC3,VB1,VB2,MB,MC,MD;
	uint16_t AC4,AC5,AC6;
	double c5,c6,mc,md,x0,x1,x2;
	double Y0,Y1,y2,p0,p1,p2;*/

	double c3,c4,b1;

	mraa_i2c_address(*i2c, BMP180_ADDR);
	if(read_int(i2c, 0xAA, &(i2c_t->AC1)) &&
	   read_int(i2c, 0xAC, &(i2c_t->AC2)) &&
	   read_int(i2c, 0xAE, &(i2c_t->AC3)) &&
   	   read_uint(i2c, 0xB0, &(i2c_t->AC4)) &&
	   read_uint(i2c, 0xB2, &(i2c_t->AC5)) &&
	   read_uint(i2c, 0xB4, &(i2c_t->AC6)) &&
	   read_int(i2c, 0xB6, &(i2c_t->VB1)) &&
	   read_int(i2c, 0xB8, &(i2c_t->VB2)) &&
	   read_int(i2c, 0xBA, &(i2c_t->MB)) &&
	   read_int(i2c, 0xBC, &(i2c_t->MC)) &&
	   read_int(i2c, 0xBE, &(i2c_t->MD))){
		
		/*printf("AC1: %d\n", i2c_t->AC1);
		printf("AC2: %d\n", AC2);
		printf("AC3: %d\n", AC3);
		printf("AC4: %d\n", AC4);
		printf("AC5: %d\n", AC5);
		printf("AC6: %d\n", AC6);
		printf("VB1: %d\n", VB1);
		printf("VB2: %d\n", VB2);
		printf("MB: %d\n", MB);
		printf("MC: %d\n", MC);
		printf("MD: %d\n", MD); */ 

		c3 = 160.0 * pow(2,-15) * i2c_t->AC3;
		c4 = pow(10,-3) * pow(2,-15) * i2c_t->AC4;
		b1 = pow(160,2) * pow(2,-30) * i2c_t->VB1;
		i2c_t->c5 = (pow(2,-15) / 160) * i2c_t->AC5;
		i2c_t->c6 = i2c_t->AC6;
		i2c_t->mc = (pow(2,11) / pow(160,2)) * i2c_t->MC;
		i2c_t->md = i2c_t->MD / 160.0;
		i2c_t->x0 = i2c_t->AC1;
		i2c_t->x1 = 160.0 * pow(2,-13) * i2c_t->AC2;
		i2c_t->x2 = pow(160,2) * pow(2,-25) * i2c_t->VB2;
		i2c_t->Y0 = c4 * pow(2,15);
		i2c_t->Y1 = c4 * c3;
		i2c_t->y2 = c4 * b1;
		i2c_t->p0 = (3791.0 -8.0) / 1600.0;
		i2c_t->p1 = 1.0 - 7357.0 * pow(2,-20);
		i2c_t->p2 = 3038.0 * 100.0 * pow(2,-36);

		return 1;
	}else{
		return 0;
	}
}
uint8_t get_pressure(mraa_i2c_context * i2c, i2c_struct_t * i2c_t, double * T, double * P){
	uint8_t data[3];
	uint8_t result;
	uint8_t subaddr = BMP180_REG_RESULT;
	double pu,s,x,y,z;
	
	mraa_i2c_address(*i2c, BMP180_ADDR);
	mraa_i2c_write_byte(*i2c,subaddr);
	result = mraa_i2c_read(*i2c, data, 3);
	if(result){
		pu = (data[0] * 256.0) + data[1] + (data[2]/256.0);
		s = *T - 25.0;
		x = (i2c_t->x2 * pow(s,2)) + (i2c_t->x1 * s) + i2c_t->x0;
		y = (i2c_t->y2 * pow(s,2)) + (i2c_t->Y1 * s) + i2c_t->Y0;
		z = (pu - x) / y;
		*P = (i2c_t->p2 * pow(z,2)) + (i2c_t->p1 * z) + i2c_t->p0;
	}
	return result;
}

uint8_t get_temperature(mraa_i2c_context * i2c, i2c_struct_t * i2c_t, double * T){
	uint8_t data[2];
	uint8_t subaddr = BMP180_REG_RESULT;
	uint8_t result;
	double tu, a;

	mraa_i2c_address(*i2c, BMP180_ADDR);
	mraa_i2c_write_byte(*i2c, subaddr);
	result = mraa_i2c_read(*i2c, data, 2);

	if(result){
		tu = (data[0] * 256.0) + data[1];

		a = i2c_t->c5 * (tu - i2c_t->c6);
		*T = a + (i2c_t->mc / (a + i2c_t->md));
	}
	return result;
}

uint8_t start_temperature(mraa_i2c_context * i2c){
	uint8_t data[2];
	uint8_t result;

	data[0] = BMP180_REG_CONTROL;
	data[1] = BMP180_COMMAND_TEMPERATURE;

	mraa_i2c_address(*i2c, BMP180_ADDR);
	result  = mraa_i2c_write(*i2c,data,2);
	if(result == MRAA_SUCCESS){
		return 5;
	}else{
		return 0;
	}
}

uint8_t start_pressure(mraa_i2c_context * i2c, uint8_t res){
	uint8_t data[2];
	uint8_t delay;

	mraa_result_t result;
	mraa_i2c_address(*i2c, BMP180_ADDR);

	data[0] = BMP180_REG_CONTROL;
	switch(res){
		case 0:
			data[1] = BMP180_COMMAND_PRESSURE0;
			delay = 5;
			break;
		case 1:
			data[1] = BMP180_COMMAND_PRESSURE1;
                        delay = 8;
	  		break;
		case 2:
			data[1] = BMP180_COMMAND_PRESSURE2;
			delay = 14;
			break;
		case 3:
			data[1] = BMP180_COMMAND_PRESSURE3;
			delay = 26;
			break;
		default:
			data[1] = BMP180_COMMAND_PRESSURE0;
			delay = 5;
			break;
	}
	result  = mraa_i2c_write(*i2c,data,2);

	if(result == MRAA_SUCCESS){
		return delay;
	}else{
		return 0;
	}
}
